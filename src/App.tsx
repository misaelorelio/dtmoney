import { useState } from 'react'
import { DashBoard } from './components/DashBoard';
import { Header } from './components/Header';
import { GlobalStyle } from './style/global';
import Modal from 'react-modal';
import { ModalTransacao } from './components/ModalTransacao';
import { TransactionsProvider } from './TransactionsContext';

Modal.setAppElement('#root');


export function App() {
  const [modalTransacao, setModalTransacao] = useState(false);

  function handleOpenModalTransacao() {
    setModalTransacao(true);
  }

  function handleCloseModalTransacao() {
    setModalTransacao(false);
  }

  return (
    <TransactionsProvider>

      <Header onOpenModalTransacao={handleOpenModalTransacao} />
      <DashBoard />
      <ModalTransacao isOpen={modalTransacao} onRequestClose={handleCloseModalTransacao} />
      <GlobalStyle />
    </TransactionsProvider>
  );
}

