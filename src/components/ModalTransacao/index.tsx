import Close from "../../assets/close.svg";
import SetaCimaImg from "../../assets/setacima.svg";
import SetaBaixoImg from "../../assets/setabaixo.svg";

import Modal from 'react-modal';
import { Container, TransactionsTypeContainer, RadioBox } from "./styles";
import { FormEvent, useContext, useState } from "react";
import { api } from "../../services/api";
import { TransactionsContext } from "../../TransactionsContext";

interface newModalTransacao {
    isOpen: boolean;
    onRequestClose: () => void;
}

export function ModalTransacao({ isOpen, onRequestClose }: newModalTransacao) {
    const {createTransaction} = useContext(TransactionsContext);
    const [title, setTitle] = useState('');
    const [amount, setAmount] = useState(0);
    const [category, setCategory] = useState('');
    const [type, setType] = useState('deposit');

    async function handleCreateNewTransacao(event: FormEvent) {
        event.preventDefault();
        await createTransaction({
            title,
            amount,
            type, 
            category
        })
        setTitle('');
        setAmount(0);
        setCategory('');
        setType('deposit');
        onRequestClose();
    }

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            overlayClassName="react-modal-overlay"
            className="react-modal-content"
        >
            <button
                type="button"
                onClick={onRequestClose}
                className="react-modal-close"
            >
                <img src={Close} alt="Fechar modal" />
            </button>
            <Container onSubmit={handleCreateNewTransacao}>
                <h2>Cadastrar transação</h2>
                <input
                    placeholder="Título"
                    value={title}
                    onChange={event => setTitle(event.target.value)}
                />
                <input
                    type="number"
                    placeholder="Valor"
                    value={amount}
                    onChange={event => setAmount(Number(event.target.value))}
                />

                <TransactionsTypeContainer>
                    <RadioBox
                        type="button"
                        onClick={() => { setType('deposit') }}
                        isActive={type === 'deposit'}
                        activeColor="green"
                    >
                        <img src={SetaCimaImg} alt="Entrada" />
                        <span>Entrada</span>
                    </RadioBox>
                    <RadioBox
                        type="button"
                        onClick={() => { setType('saque') }}
                        isActive={type === 'saque'}
                        activeColor="red"
                    >
                        <img src={SetaBaixoImg} alt="Saida" />
                        <span>Saída</span>
                    </RadioBox>

                </TransactionsTypeContainer>
                <input
                    placeholder="Categoria"
                    value={category}
                    onChange={event => setCategory(event.target.value)}
                />

                <button type="submit">
                    Cadastrar
                </ button>
            </Container>
        </Modal>
    );
}