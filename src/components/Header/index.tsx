import logoImage from '../../assets/logo.svg';
import { Container, Content } from './styles';

interface HeaderProps {
    onOpenModalTransacao: () => void;
}


export function Header({ onOpenModalTransacao }: HeaderProps) {


    return (
        <Container>
            <Content>
                <img src={logoImage} alt="dt money" />
                <button type="button" onClick={onOpenModalTransacao}>
                    Nova transação
                </button>

            </Content>
        </Container>
    );
}