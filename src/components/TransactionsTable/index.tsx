import axios from "axios";
import { useContext, useEffect } from "react";
import { TransactionsContext } from "../../TransactionsContext";
import { Container } from "./styles";

export function TransactionsTable(){
    const {transactions} = useContext(TransactionsContext);

    async function dadosApi() {
        const  data =   await axios.get('localhost:8080/transactions');
        console.log(data)
        return data;
        
    }
useEffect(() => {
    dadosApi();
}, [])


    return(
        <Container>
            <table>
                <thead>
                    <th>Título</th>
                    <th>Valor</th>
                    <th>Categoria</th>
                    <th>Data</th>
                </thead>
                <tbody>
                   {transactions.map(transaction =>{
                       return(
                        <tr key={transaction.id}>
                        <td>{transaction.title}</td>
                        <td className={transaction.type}>
                            {new Intl.NumberFormat('pt-BR', {
                                style: 'currency',
                                currency: 'BRL'
                            }).format(transaction.amount)}</td>
                        <td>{transaction.category}</td>
                         <td>{new Intl.DateTimeFormat('pt-BR').format(new Date(transaction.createdAt))}</td> 
                        
                    </tr>
                       );
                   })}
                </tbody>
            </table>
        </Container>
    );
}
